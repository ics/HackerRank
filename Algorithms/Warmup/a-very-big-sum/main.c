#include <stdio.h>

int	main(void)
{
	int	i;
	int	n;
	scanf("%d", &n);
	int	arr[n];
	long	sum;

	i = 0;
	while (i < n)
	{
		scanf("%d", &arr[i]);
		i++;
	}

	sum = 0;
	i = 0;
	while (i < n)
	{
		sum += arr[i];
		i++;
	}
	printf("%ld\n", sum);
	return (0);
}
