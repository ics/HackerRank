#include <stdio.h>

int	difference(int a, int b)
{
	if (a < b)
		return (b - a);
	else
		return (a - b);
}

int	diagonals_dif(int n, int arr[n][n])
{
	int	i;
	int	sum_primary;
	int	sum_secondary;
	sum_primary = 0;
	sum_secondary = 0;
	i = 0;
	while (i < n)
	{
		sum_primary += arr[i][i];
		i++;
	}
	i = 0;
	while (i < n)
	{
		sum_secondary += arr[i][n - 1 - i];
		i++;
	}
	return (difference(sum_primary, sum_secondary));
}

int	main(void)
{
	int	n;
	scanf("%d", &n);

	int	arr[n][n];
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			scanf("%d", &arr[i][j]);
	}


	printf("%d\n", diagonals_dif(n, arr));
	return (0);
}
