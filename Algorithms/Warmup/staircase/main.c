#include <stdio.h>
#include <unistd.h>

void	staircase(int n, int step)
{
	int	i;

	if (n == step)
		return ;
	i = 0;
	while (i < n)
	{
		if (i < n - step - 1)
			write(1, " ", 1);
		else
			write(1, "#", 1);
		i++;
	}
	write(1, "\n", 1);
	staircase(n, step + 1);
}

int	main(void)
{
	int	n;

	scanf("%d", &n);
	staircase(n, 0);
	return (0);
}
