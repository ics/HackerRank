#include <stdio.h>

void	compare(int alice, int bob, int *score)
{
	if (alice > bob)
		score[0] += 1;
	if (alice < bob)
		score[1] += 1;
}

int	main(void)
{
	int	a0;
	int	a1;
	int	a2;
	int	b0;
	int	b1;
	int	b2;
	int	score[2];

	scanf("%d %d %d", &a0, &a1, &a2);
	scanf("%d %d %d", &b0, &b1, &b2);
	score[0] = 0;
	score[1] = 0;
	compare(a0, b0, score);
	compare(a1, b1, score);
	compare(a2, b2, score);
	printf("%d %d\n", score[0], score[1]);
	return (0);
}
